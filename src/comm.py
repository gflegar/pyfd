import time
from threading import Thread, Lock
from queue import Queue
from types import SimpleNamespace
from mpi4py import MPI


import utils


class BaseCommAdapter(object):
    def __init__(self, comm, logging=False):
        self.logging = logging
        self._comm = comm

    def __getattr__(self, name):
        return getattr(self._comm, name)

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def ssend(self, msg, dest, tag=0):
        self.ssend(msg, dest=dest, tag=tag)

    def srecv(self, source, tag=0):
        return self.srecv(source=source, tag=tag)

    def isfaulty(self, procid):
        return self._comm.isfaulty(procid)

    def _log(self, msg, *args, **kwargs):
        if self.logging:
            M = '{:15}: {}'.format(type(self).__name__, msg)
            utils.log(M, *args, **kwargs)


class MPICommAdapter(BaseCommAdapter):
    def __init__(self, comm, logging=False):
        super().__init__(comm, logging=logging)
        size = comm.Get_size()
        self._queues = [Queue() for i in range(size)]
        self._to_close = size - 1
        self._recv_thread = Thread(target=self._recv_daemon)
        self.__send_lock = Lock()

    def open(self):
        self._log('Starting receiver thread')
        self._recv_thread.start()

    def close(self):
        self._log('Closing communicator')
        for i in range(self.Get_size()):
            if i != self.Get_rank():
                self._comm.send(('ctl', 'exit'), dest=i)
        self._log('Waiting for receiver thread to finish')
        self._recv_thread.join()

    def send(self, msg, dest, tag=0):
        with self.__send_lock:
            self._comm.send(('app', msg), dest=dest)

    def recv(self, source, tag=0):
        return self._queues[source].get()

    ssend = send

    srecv = recv

    def isfaulty(self, procid):
        return False

    def _recv_daemon(self):
        self._log('Receiver thread started')
        status = MPI.Status()
        while True:
            self._log(self._to_close)
            if self._to_close == 0:
                break
            t, msg = self._comm.recv(source=MPI.ANY_SOURCE, status=status,
                                     tag=MPI.ANY_TAG)
            source = status.Get_source()
            self._log('Received {} message "{}" from {}'
                      .format(t, msg, source))
            if t == 'ctl' and msg == 'exit':
                self._to_close -= 1
            elif t == 'app':
                self._queues[source].put(msg)


COMM_WORLD = MPICommAdapter(MPI.COMM_WORLD, logging=False)


class LogAdapter(BaseCommAdapter):
    SEND_FMT='{:15}: Sending  "{}" to   {} (tag: {})'
    RECV_FMT='{:15}: Received "{}" from {} (tag: {})'

    def __init__(self, comm, prefix=''):
        super().__init__(comm)
        self._prefix = prefix

    def send(self, msg, dest, tag=0):
        utils.log(type(self).SEND_FMT.format(self._prefix, msg, dest, tag))
        self._comm.send(msg, dest=dest, tag=tag)

    def recv(self, source, tag=0):
        msg = self._comm.recv(source=source, tag=tag)
        utils.log(type(self).RECV_FMT.format(self._prefix, msg, source, tag))
        return msg

    def ssend(self, msg, dest, tag=0):
        utils.log(type(self).SEND_FMT.format(self._prefix, msg, dest, tag))
        self._comm.ssend(msg, dest=dest, tag=tag)

    def srecv(self, source, tag=0):
        msg = self._comm.srecv(source=source, tag=tag)
        utils.log(type(self).RECV_FMT.format(self._prefix, msg, source, tag))
        return msg


class FaultyAdapter(BaseCommAdapter):
    def __init__(self, comm, faults, logging=False):
        """ Create a faulty communicator with transmission delays and
        termination after given time. """
        super().__init__(comm, logging=logging)
        self._faults = faults.get(str(comm.Get_rank()), {'delay': 0})
        self._msgq = Queue()
        self._sender_thread = Thread(target=self._msg_sender)

    def open(self):
        self._comm.open()
        self._log('Starting sender thread')
        self._sender_thread.start()

    def close(self):
        self._log('Stopping sender thread')
        self._msgq.put(None)
        self._sender_thread.join()
        self._log('Sender thread stopped')
        self._comm.close()

    def send(self, msg, dest, tag=0):
        """ Send """
        MSG = ('Adding message "{}" (dest: {}, tag: {}) to queue ({})')
        self._log(MSG.format(msg, dest, tag, self._msgq.qsize()))
        self._msgq.put((msg, dest, tag, utils.time()))

    def ssend(self, msg, dest, tag=0):
        MSG = ('Adding secure message "{}" (dest: {}, tag: {}) to queue ({})')
        self._log(MSG.format(msg, dest, tag, self._msgq.qsize()))
        self._msgq.put((msg, dest, tag, None))

    def _msg_sender(self):
        while True:
            data = self._msgq.get()
            if data is None: break
            msg, dest, tag, t = data
            if t is None:
                self._comm.ssend(msg, dest=dest, tag=tag)
            elif 'fault' not in self._faults or self._faults['fault'] > t:
                diff = utils.time() - t
                self._log('"{}" waited for {}s'.format(msg, diff))
                if diff < self._faults['delay']:
                    self._log('"{}" waiting for additional {}s'
                              .format(msg, self._faults['delay'] - diff))
                    time.sleep(self._faults['delay'] - diff)
                self._comm.send(msg, dest=dest, tag=tag)
            else:
                self._log('FAULT: message not sent')


class AFDAdapter(BaseCommAdapter):
    def __init__(self, comm, logging=False):
        super().__init__(comm, logging=logging)
        self._data_lock = Lock()
        self._data = [
            SimpleNamespace(
                delay=0.0,
                pending=set(),
                queue=Queue(),
                listener=Thread(target=self._listen, args=(i,))
            )
            for i in range(comm.Get_size())
        ]

    def open(self):
        self._comm.open()
        self._log('Starting message listeners')
        for i, pd in enumerate(self._data):
            if i != self.Get_rank():
                pd.listener.start()
        self._log('Listeners started')

    def close(self):
        for i in range(self._comm.Get_size()):
            if i != self.Get_rank():
                self._comm.ssend(('exit', None, None), dest=i, tag=0)
        self._log('Closing listeners')
        for i, pd in enumerate(self._data):
            if i != self.Get_rank():
                pd.listener.join()
        self._log('Listeners closed')
        self._comm.close()

    def send(self, msg, dest, tag=0):
        tstamp = utils.time()
        with self._data_lock:
            self._data[dest].pending.add(tstamp)
        M = ('app', tstamp, msg)
        self._comm.send(M, dest=dest, tag=0)
        self._log_fd_state()

    def recv(self, source, tag=0):
        return self._data[source].queue.get()

    def isfaulty(self, procid):
        if (self._comm.Get_rank() == procid):
            return False
        tstamp = utils.time()
        with self._data_lock:
            if len(self._data[procid].pending) == 0:
                delay = 0.0
                self._data[procid].pending.add(tstamp)
                self._comm.send(('ping', tstamp, None), dest=procid, tag=0)
            else:
                delay = tstamp - min(self._data[procid].pending)
            max_delay = self._data[procid].delay
        self._log_fd_state()
        return delay > max_delay

    def _listen(self, procid):
        data = self._data[procid]
        self._log('Listening for messages from proc {}'.format(procid))
        while True:
            M = self._comm.recv(source=procid, tag=0)
            msg_type, tstamp, msg = M
            if msg_type == 'app':
                self._log('Queued "{}" ({})'.format(msg, data.queue.qsize()+1))
                data.queue.put(msg)
                self._comm.send(('ack', tstamp, None), dest=procid, tag=0)
            elif msg_type == 'ping':
                self._comm.send(('ack', tstamp, None), dest=procid, tag=0)
            elif msg_type == 'ack':
                with self._data_lock:
                    data.pending.remove(tstamp)
                    delay = utils.time() - tstamp
                    data.delay = max(data.delay, delay)
            elif msg_type == 'exit':
                break
            else:
                self._log('ERROR: Unknown message type')
            self._log_fd_state()

    def _log_fd_state(self):
        FMT = '(#pending: {}, delay: {:.6f}, max_delay: {:.6f})'
        tstamp = utils.time()
        self._log(' '.join(FMT.format(
                len(pd.pending),
                tstamp - min(pd.pending, default=tstamp),
                pd.delay)
            for pd in self._data))

