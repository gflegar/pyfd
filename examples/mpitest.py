import sys
import time
import json
from mpi4py import MPI


sys.path.append('../src/')
import utils
from comms import MsgLogger, FaultyComm, AdaptiveFDComm


def create_comm():
    comm = MPI.COMM_WORLD

    comm = MsgLogger('FaultyComm', comm)
    with open('config.json') as fp:
        config = json.load(fp)
    comm = FaultyComm(comm, config['comm'], logging=True)
    comm = MsgLogger('AdaptiveFDComm', comm)
    comm = AdaptiveFDComm(comm, logging=True)

    return comm


def main():
    utils.set_comm(create_comm())
    comm = utils.comm()

    if utils.procid() == 0:
        while True:
            msg = utils.time()
            utils.pt('Sending msg "{}"'.format(msg))
            comm.send(msg, dest=1, tag=1)
            time.sleep(0.5)

    elif utils.procid() == 1:
        while True:
            msg = comm.recv(source=0, tag=1)
            utils.pt('Received msg "{}"'.format(msg))
            time.sleep(0.5)


def usage():
    print('Usage: mpirun -np 2 python3 {} [1|2]'.format(sys.argv[0]),
          file=sys.stderr)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        usage()
    else:
        call = {'1': main}
        call.get(sys.argv[1], usage)()

