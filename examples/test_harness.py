import sys
import argparse
import json

sys.path.append('../src/')
import utils
from comm import COMM_WORLD, LogAdapter, FaultyAdapter, AFDAdapter


def SimpleComm(logging=0):
    comm = COMM_WORLD
    if logging >= 1:
        comm = LogAdapter(comm, 'App')
    return comm


def FaultyComm(conf={}, logging=0):
    comm = COMM_WORLD
    if logging >= 2:
        comm = LogAdapter(comm, 'FaultyAdapter')
    comm = FaultyAdapter(comm, conf, logging=(logging>=2))
    if logging >= 1:
        comm = LogAdapter(comm, 'App')
    return comm


def AFDComm(conf={}, logging=0):
    comm = COMM_WORLD
    if logging >= 3:
        comm = LogAdapter(comm, 'FaultyAdapter')
    comm = FaultyAdapter(comm, conf, logging=(logging>=3))
    if logging >= 2:
        comm = LogAdapter(comm, 'AFDAdapter')
    comm = AFDAdapter(comm, logging=(logging>=2))
    if logging >= 1:
        comm = LogAdapter(comm, 'App')
    return comm


def parse_args(comms, default):
    prog = 'mpirun -n N python3 {}'.format(sys.argv[0])
    parser = argparse.ArgumentParser(prog=prog)
    parser.add_argument('-l', '--log', type=int, choices=range(4), default=0)
    parser.add_argument('-c', '--comm', choices=comms, default=default)
    return parser.parse_args()


def run(method):
    args = parse_args(['simple', 'faulty', 'afd'], 'simple')
    with open('config.json') as fp:
        config = json.load(fp)
    comms = {
        'simple': SimpleComm(logging=args.log),
        'faulty': FaultyComm(conf=config['comm'], logging=args.log),
        'afd': AFDComm(conf=config['comm'], logging=args.log)
    }
    with comms[args.comm] as comm:
        utils.set_comm(comm)
        method(comm)


if __name__ == '__main__':
    def hello(comm):
        utils.pt('Test harnes working for proc {}'.format(comm.Get_rank()))
    run(hello)

