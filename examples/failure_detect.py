import sys
import argparse
import json
import time
from threading import Thread, Event


sys.path.append('../src/')
import utils


import test_harness


def main(comm):
    ALLOWED_SUSPECTS = 10

    done = Event()
    prev = (comm.Get_rank() + comm.Get_size() - 1) % comm.Get_size()
    next_ = (comm.Get_rank() + 1) % comm.Get_size()

    def thread_job():
        while True:
            msg = comm.recv(source=prev)
            if done.is_set():
                break
            if msg == 'exit':
                done.set()
                break
            elif msg == 'token':
                utils.pt('Token at {}'.format(comm.Get_rank()))
                time.sleep(0.3)
                comm.send('token', dest=next_)
            else:
                utils.pt('Unknown message!')
                done.set()
                break

    job = Thread(target=thread_job, daemon=True)
    job.start()

    if comm.Get_rank() == 0:
        utils.pt('Token at 0')
        comm.send('token', dest=next_)

    faults = 0
    while faults < ALLOWED_SUSPECTS:
        if done.wait(0.3):
            utils.pt('Got exit signal')
            break
        if comm.isfaulty(prev):
            faults += 1
            utils.pt('Proc {} FAULT ({})'.format(prev, faults))
        else:
            faults = 0
    done.set()
    comm.send('exit', dest=next_)
    utils.pt('Exiting!')


if __name__ == '__main__':
    test_harness.run(main)

