import sys
import argparse
import json
import time


sys.path.append('../src/')
import utils


import test_harness


def main(comm):
    prev = (comm.Get_rank() + comm.Get_size() - 1) % comm.Get_size()
    next_ = (comm.Get_rank() + 1) % comm.Get_size()
    if comm.Get_rank() == 0:
        utils.pt('Token at {}'.format(comm.Get_rank()))
        time.sleep(0.3)
        comm.send('token', dest=next_)

    while True:
        msg = comm.recv(source=prev)
        utils.pt('Token at {}'.format(comm.Get_rank()))
        time.sleep(0.3)
        comm.send('token', dest=next_)

if __name__ == '__main__':
    test_harness.run(main)

