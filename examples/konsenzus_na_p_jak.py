import sys
import argparse
import json
import time
import itertools
from threading import Thread, Event
from queue import Queue
import random
sys.path.append('../src/')
import utils
import test_harness


from enum import Enum
class tip_poruke(Enum):
    prijedlog = 0
    aproksimacija = 1
    ack = 2
    nack = 3
    dogovor = 4


def slanje_na_j(j, comm, spremnik):
    while True:
        while not spremnik.empty():
            kome, poruka = spremnik.get()
            comm.send(poruka, dest=kome)
        time.sleep(0.1)

def primanje_sa_j(j, comm, spremnik):
    while True:
        spremnik.put(comm.recv(source=j))


# vraca par (postignut konsenzus : bool, vrijednost_konsenzusa : int)
def obradi_dolazne_poruke(buffer_dolazne, prijedlozi, koordinatorove_aproksimacije, ackovi, nackovi):
    while not buffer_dolazne.empty():
        tip, podaci = buffer_dolazne.get()
        if tip == tip_poruke.prijedlog:
            id_j, runda_j, estimate_j, ts_j = podaci
            prijedlozi_runde_j = prijedlozi.setdefault(runda_j, dict())
            prijedlozi_sa_ts_j = prijedlozi_runde_j.setdefault(ts_j, list())
            prijedlozi_sa_ts_j.append(estimate_j)
        elif tip == tip_poruke.aproksimacija:
            id_j, runda_j, estimate_j = podaci
            koordinatorove_aproksimacije[runda_j] = estimate_j
        elif tip == tip_poruke.ack:
            id_j, runda_j = podaci
            za_rundu_j = ackovi.setdefault(runda_j, set())
            za_rundu_j.add(id_j)
        elif tip == tip_poruke.nack:
            id_j, runda_j = podaci
            za_rundu_j = nackovi.setdefault(runda_j, set())
            za_rundu_j.add(id_j)
        elif tip == tip_poruke.dogovor:
            return True, podaci # postignut konsenzus

    return False, None

def main(comm):
    estimate = comm.Get_rank()
    ts = 0
    konsenzus_postignut  = False
    konsenzus_vrijednost = None
    vecina = (comm.Get_size() + 2) // 2
    buffer_dolazne, buffer_odlazne = Queue(), Queue()
    prijedlozi = dict()                 # int za_rundu => dict(int ts => list<int prijedlog>)
    aproksimacije = dict()              # int za_rundu => int aproksimacija
    ackovi, nackovi = dict(), dict()    # int za_rundu => set(int id_procesa)


    for j in range(comm.Get_size()):
        if comm.Get_rank() == j:
            continue
        Thread(target=primanje_sa_j, daemon=True, args=(j, comm, buffer_dolazne)).start()
        Thread(target=slanje_na_j,   daemon=True, args=(j, comm, buffer_odlazne)).start()

    for runda in itertools.count():
        koordinator = runda % comm.Get_size()

        # faza slanja
        if koordinator != comm.Get_rank():
            buffer_odlazne.put(
                (
                    koordinator,
                    (
                        tip_poruke.prijedlog,
                        (comm.Get_rank(), runda, estimate, ts)
                    )
                )
            )

        # faza usuglasavanja
        if koordinator == comm.Get_rank():
            utils.pt('Koordinator {} ceka na dovoljan broj prijedloga u rundi {}.'.format(comm.Get_rank(), runda))
            prijedlozi.setdefault(runda, dict())
            prijedlozi[runda].setdefault(ts, list())
            prijedlozi[runda][ts].append(estimate)
            while True:
                konsenzus_postignut, konsenzus_vrijednost =\
                    obradi_dolazne_poruke(buffer_dolazne, prijedlozi, aproksimacije, ackovi, nackovi)
                if konsenzus_postignut:
                    break
                elif runda in prijedlozi:
                    broj_prijedloga = sum([len(lista_za_ts) for ts, lista_za_ts in prijedlozi[runda].items()])
                    if broj_prijedloga >= vecina:
                        utils.pt('Koordinator {} je primio dovoljan broj prijedloga u rundi {}.'.format(comm.Get_rank(), runda))
                        max_ts = max(prijedlozi[runda].keys())
                        estimate = prijedlozi[runda][max_ts][len(prijedlozi[runda][max_ts]) - 1]
                        ts = runda
                        for j in range(comm.Get_size()):
                            if comm.Get_rank() == j:
                                continue
                            buffer_odlazne.put(
                                (
                                    j,
                                    (
                                        tip_poruke.aproksimacija,
                                        (comm.Get_rank(), runda, estimate)
                                    )
                                )
                            )
                        break
                time.sleep(0.1)
        if konsenzus_postignut:
            break

        # faza cekanja
        if koordinator != comm.Get_rank():
            utils.pt('Proces {} ceka aproksimaciju u rundi {}.'.format(comm.Get_rank(), runda))
            while True:
                konsenzus_postignut, konsenzus_vrijednost = \
                    obradi_dolazne_poruke(buffer_dolazne, prijedlozi, aproksimacije, ackovi, nackovi)
                if konsenzus_postignut:
                    break
                elif runda in aproksimacije:
                    utils.pt('Proces {} je primio aproksimaciju u rundi {}, salje ACK.'.format(comm.Get_rank(), runda))
                    estimate = aproksimacije[runda]
                    ts = runda
                    buffer_odlazne.put(
                        (
                            koordinator,
                            (
                                tip_poruke.ack,
                                (comm.Get_rank(), runda)
                            )
                        )
                    )
                    break
                elif comm.isfaulty(koordinator):
                    utils.pt('Proces {} u rundi {} sumnja na koordinatora, salje NACK.'.format(comm.Get_rank(), runda))
                    buffer_odlazne.put(
                        (
                            koordinator,
                            (
                                tip_poruke.nack,
                                (comm.Get_rank(), runda)
                            )
                        )
                    )
                    break
                #else:
                #    utils.pt("comm.isfaulty(koordinator) = false, kaze proc {}".format(comm.Get_rank()))
                time.sleep(0.1)
        if konsenzus_postignut:
            break

        # faza postizanja konsenzusa
        if koordinator == comm.Get_rank():
            utils.pt('Koordinator {} ceka (N)ACK-ove u rundi {}.'.format(comm.Get_rank(), runda))
            ackovi.setdefault(runda, set())
            ackovi[runda].add(koordinator)

            while True:
                konsenzus_postignut, konsenzus_vrijednost = \
                    obradi_dolazne_poruke(buffer_dolazne, prijedlozi, aproksimacije, ackovi, nackovi)
                if konsenzus_postignut:
                    break
                elif runda in ackovi or runda in nackovi:
                    broj_prijedloga = 0
                    if runda in ackovi:
                        broj_prijedloga += len(ackovi[runda])
                    if runda in nackovi:
                        broj_prijedloga += len(nackovi[runda])
                    if broj_prijedloga >= vecina:
                        if len(ackovi[runda]) >= vecina: # hm, a sto ako samo jos nisu stigli ostali ackovi?
                            utils.pt('Koordinator {} je primio dovoljno ACK-ova u rundi {}, salje dogovor.'.format(comm.Get_rank(), runda))
                            konsenzus_postignut = True
                            konsenzus_vrijednost = estimate
                            for j in range(comm.Get_size()):
                                if comm.Get_rank() == j:
                                    continue
                                buffer_odlazne.put(
                                    (
                                        j,
                                        (
                                            tip_poruke.dogovor,
                                            estimate
                                        )
                                    )
                                )
                            break
                time.sleep(0.1)
        if konsenzus_postignut:
            break

    utils.pt('Proces {} je postignuo konsenzus {}.'.format(comm.Get_rank(), konsenzus_vrijednost))

    while not buffer_odlazne.empty():
        time.sleep(1)

if __name__ == '__main__':
    test_harness.run(main)
