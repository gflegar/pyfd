import sys
import argparse
import json


sys.path.append('../src/')
import utils


import test_harness


def main(comm):
    if comm.Get_rank() == 0:
        for i in range(1, comm.Get_size()):
            msg = comm.recv(source=i, tag=0)
            utils.pt('Message from {}: {}'.format(i, msg))
    else:
        comm.send('Hello from {}!'.format(comm.Get_rank()), dest=0, tag=0)


if __name__ == '__main__':
    test_harness.run(main)

