import sys
from threading import Thread, Lock
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

recv_lock = Lock()

def receive_msg(source):
    msg = comm.recv(source=source)
    print('PROC {}: Received "{}" from {}'.format(rank, msg, source))

threads = [Thread(target=receive_msg, args=(i,))
           for i in range(size)
           if i != rank]

for t in threads:
    t.start()

for i in range(size):
    if i != rank:
        print('PROC {}: Sending "Hello" to {}'.format(rank, i))
        comm.send('Hello', dest=i)

for t in threads:
    t.join()

print('PROC {}: Done!'.format(rank))

